
describe('Tickets', () => {
    beforeEach(() => cy.visit('http://ticket-box.s3.eu-central-1.amazonaws.com/index.html'))

    it("has ticket box header's heading", () => {
        const firstName = 'Gustavo'
        const lastName = 'Silva' 
        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('Jose@gmail.com')
        cy.get('#requests').type('Pra cima vozao!!!')
        cy.get('#signature').type(`${firstName} ${lastName}`)
    })

    it('Select two tickets', () => {
        cy.get('#ticket-quantity').select(0) // selecionando uma opção num campo do tipo select
    })

    it('Select vip ticket type', () => { // selecionando campo do tipo radioButton
        cy.get('#vip').check()
    })

    it("Selects 'social media' checkbox", () => {
        cy.get('#social-media').check()
    })

    it('Select Friend and Publication', () => {
        cy.get('#friend').check()
        cy.get('#publication').check()
        cy.get('#friend').uncheck()        
    })

    it('fills all the text input fields', () => {

        cy.get('header h1').should('contain','TICKETBOX')      
    })

    it('Alerts on invalid email', () => {
        cy.get('#email')
        .as('email')
        .type('test invalid email')

        cy.get('#email.invalid').should('exist') // o .invalid procura um id email da classe invalid

        // o @ é utilizado para identificar um apelido que foi dado atraves da função 'as'
        cy.get('@email')
            .clear() // limpa o campo
            .type('Gustavo@gmail.com')

        cy.get('#email.invalid').should('not.exist')
    })

    it('Fills and reset the form', () => {
        const firstName = 'Gustavo'
        const lastName = 'Silva' 
        const fullName = `${firstName} ${lastName}`

        cy.get('#first-name').type(firstName)
        cy.get('#last-name').type(lastName)
        cy.get('#email').type('Jose@gmail.com')
        cy.get('#ticket-quantity').select("2")
        cy.get('#vip').check()
        cy.get('#friend').check()
        cy.get('#requests').type('Pra cima vozão!!!')

        cy.get('.agreement p').should(
            'contain', 
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        )

        cy.get('#agree').click()
        cy.get('#signature').type(fullName)
               
        cy.get("button[type='submit']")
            .as('submitButton')
            .should('not.be.disabled') // função do should q permite verificar se o botao esta desabilitado

        cy.get("button[type='reset']").click()    
        
        cy.get('@submitButton').should('be.to.disabled')
    })

    it('Fills mandatory fields using support', () => {
        const customer = {
            firstName:  'Gustavo',
            lastName:   'Silva',
            email:  'GustavoSilva@gmail.com'
        }

        cy.fillMandatoryFields(customer)
       
        cy.get("button[type='submit']")
            .as('submitButton')
            .should('not.be.disabled')

        cy.get("#agree").uncheck()    
        
        cy.get('@submitButton').should('be.to.disabled')
    })
})